/*
 * LATIMImageAcquirer.cpp
 *
 *  Created on: May 22, 2015
 *      Author: servio
 */

#include "LATIMImageAcquirer.h"

#include <iostream>

using namespace cv;

/**@brief Take one capture from the camera (a Frame), if the id is not specified, the ID takes the default value 0
 * The function will check if the initialization is succeed.
 *@param path, the path where the image will be saved.
 *@param idCamera, ID of the camera that will be used.
 *@return Int, the function return CAMCap_FAILURE if is no possible to open the camera device and CAMCap_SUCCESS if everything is okey.
 */
int LATIMImageAcquirer::takePhoto(char* path, int idCamera) {
	int status = CAMCap_SUCCESS;
	VideoCapture cap(idCamera);
	if (!cap.isOpened()) {
		status = CAMCap_FAILURE; //initialization is succeed
	}
	if (status != CAMCap_FAILURE) {
		Mat frame;
		cap >> frame;
		imwrite(path, frame);
	}
	return status;
	// the camera will be deinitialized automatically in VideoCapture destructor
}

/**@brief Take one video from the camera, if the id is not specified, the ID takes the default value 0
 * The function will check if the initialization is succeed.
 *@param path, the path where the image will be saved.
 *@param idCamera, ID of the camera that will be used.
 *@return Int, the function return CAMCap_FAILURE if is no possible to open the camera device and CAMCap_SUCCESS if everything is okey.
 */
int LATIMImageAcquirer::takeVideo(char* path, int idCamera) {
	VideoCapture cap(idCamera); // open the default camera
	if (!cap.isOpened()) {  // check if we succeeded
		return CAMCap_FAILURE;
	}
	for (;;) {
		Mat frame;
		cap >> frame; // get a new frame from camera
		// do any processing
		imwrite(path, frame);
		if (waitKey(30) >= 0) {
			break;   // you can increase delay to 2 seconds here
		}
	}
	return CAMCap_SUCCESS;
	// the camera will be deinitialized automatically in VideoCapture destructor
}

/**@brief Load a image from the file specified.
 *@param path, the path where the image is, the path must include the image's name.
 *@param mat, the Mat struct that store the read image.
 *@return Int, the function return CAMCap_FAILURE if is no possible to open the camera device and CAMCap_SUCCESS if everything is okey.
 */
int LATIMImageAcquirer::takePhotoFromFile(char* path, Mat *mat) {
	int status = CAMCap_SUCCESS;
	Mat image;
	image = imread(path, CV_LOAD_IMAGE_COLOR);   // Read the file

	if (!image.data)                              // Check for invalid input
	{
		status = CAMCap_FAILURE;
	}
	if (status != CAMCap_FAILURE) {
		*mat = image;
	}
	return status;
}

