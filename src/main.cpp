//============================================================================
// Name        : mian.cpp
// Author      : Servio Paguada
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

#include "LATIMImageAcquirer/LATIMImageAcquirer.h"

using namespace std;

int main(int narg, char *args[]) {

	LATIMImageAcquirer cam;
	char path[] = "prueba2.jpg";
	if(cam.takePhoto(path, 0) == CAMCap_FAILURE)
		cout<<"The test has failed";
	else
		cout<<"Test passed";

	return 0;

}
