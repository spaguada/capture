/*
 * LATIMImageAcquirer.h
 *
 *  Created on: May 22, 2015
 *      Author: servio
 */

#ifndef LATIMIMAGEACQUIRER_H_
#define LATIMIMAGEACQUIRER_H_

#include <opencv2/opencv.hpp>

using namespace cv;

enum {
	CAMCap_FAILURE, CAMCap_SUCCESS
};

class LATIMImageAcquirer {
public:
	int takePhoto(char* path, int idCamera = 0);
	int takeVideo(char* path, int idCamera = 0);
	int takePhotoFromFile(char* path, Mat *mat);
};

#endif /* LATIMIMAGEACQUIRER_H_ */
